#!/bin/sh

export INTERNAL_HOST_IP=$(curl -m 5 -s 169.254.169.254/latest/meta-data/local-ipv4)

if [[ $? -ne 0 ]]; then
    export INTERNAL_HOST_IP=${HOST_IP}
fi

if [ -z ${JMX_PORT} ]; then 
	export JMX_PORT=9010
fi
 
java $JAVA_OPTS -Dcom.sun.management.jmxremote=true -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.rmi.port=${JMX_PORT} -Dcom.sun.management.jmxremote.port=${JMX_PORT} -Dcom.sun.management.jmxremote.local.only=false -Djava.rmi.server.hostname=$INTERNAL_HOST_IP -jar -Dlog4j.configurationFile=file:/opt/channelvaluebytesjob/log4j2.properties /opt/channelvaluebytesjob/channelvaluebytesjob-fat.jar -f $FLEET