package com.trimble.r2m.channelvaluebytesjob;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.name.Names;
import com.nexala.spectrum.binary.provider.ConfigurationProvider;
import com.nexala.spectrum.binary.provider.DataSourceProviderInterface;
import com.trimble.r2m.channelvaluebytesjob.application.Configuration;
import com.trimble.r2m.channelvaluebytesjob.provider.DataSourceProvider;

public class ChannelValueBytesModule  extends AbstractModule{
	
	protected void configure() {
		binder().bind(Configuration.class);
		bind(ConfigurationProvider.class).annotatedWith(Names.named("dbConfig")).to(Configuration.class);
		bind(DataSourceProviderInterface.class).to(DataSourceProvider.class).in(Scopes.SINGLETON);
	}

}
