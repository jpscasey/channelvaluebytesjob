package com.trimble.r2m.channelvaluebytesjob;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.nexala.spectrum.binary.bean.ChannelDataBinary;
import com.nexala.spectrum.binary.caching.ChannelBinaryCache;
import com.nexala.spectrum.binary.parse.ChannelValueParser;
import com.nexala.spectrum.binary.provider.ChannelValueBinaryProvider;

/**
 * update channel value bytes with blob data - one off job
 * @author John Casey
 */

public class App {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) {
		try {
			LOGGER.info("Starting channel value bytes job");
			Injector injector = Guice.createInjector(new ChannelValueBytesModule());
		
			Options options = new Options();
			options.addOption(Option.builder("f").desc("fleet").required(true).hasArg(true).build());
        
			CommandLineParser parser = new DefaultParser();
			CommandLine cmd = null;
			
			// parse command line args
			try {
				cmd = parser.parse(options, args);
			} catch (ParseException e) {
				LOGGER.info("Exiting program. Please specify fleet.");
				System.exit(0);//Exit program
			}
        
			String fleetId = null;
			if (cmd.hasOption("f")) {
				fleetId = cmd.getOptionValue("f").toUpperCase();
			}
			
			LOGGER.info("Fleet entered: " + fleetId);
			if(!fleetId.equals("SNG")) {
				LOGGER.info("Exiting program. This job is currently only available for SNG fleet");
				System.exit(0);//Exit program
			}
        
			// init cache
			LOGGER.info("Initialising cache");
			ChannelBinaryCache channelCache = injector.getInstance(ChannelBinaryCache.class);
			LOGGER.info(ChannelBinaryCache.test());
        
			// get data using args
			List<ChannelDataBinary> channelData = new ArrayList<ChannelDataBinary>();
        
			final ChannelValueBinaryProvider dataProvider = injector.getInstance(ChannelValueBinaryProvider.class);
			long minId = -1;
			LOGGER.info("Getting initial min channel id");
			try {
				minId = dataProvider.getChannelDataMinId(fleetId); //set minId to -1 if no data to process
			} catch (Exception e) {
				LOGGER.info(e.getMessage());
			}
			LOGGER.info("initial min channel id = " + minId);

			while(minId != -1) {
				try {
					LOGGER.info("Getting batch of channel values");
					channelData = dataProvider.getChannelData(fleetId, minId);
					LOGGER.info("Queried " + String.valueOf(channelData.size()) + " channel values.");
				} catch (Exception e) {
					LOGGER.info(e.getMessage());
				}
        
				ChannelValueParser channelParser = new ChannelValueParser(channelCache);
				LOGGER.info("Encoding bytes for data set");
				channelData = channelParser.encodeDataSet(fleetId, channelData);
				LOGGER.info("bytes encoded for data set");

				LOGGER.info("updating bytes for data set");
				for(ChannelDataBinary entry : channelData) {
					try {
						dataProvider.updateBytes(fleetId, entry.getBytes(), entry.getChannelValueId());
					} catch (Exception e) {
						LOGGER.info(e.getMessage());
					}
        			
				}
				try {
					LOGGER.info("getting another min channel id");
					minId = dataProvider.getChannelDataMinId(fleetId); // set minId to -1 when no more data to process
					LOGGER.info("min channel id = " + minId);
				} catch (Exception e) {
					LOGGER.info(e.getMessage());
				}
			}
			LOGGER.info("Finishing channel value bytes job");
		} catch (Exception e) {
			LOGGER.info("Abnormal program termination. Please double check environment variables are correct");
			LOGGER.info(e.getMessage());
		}
	}

}
