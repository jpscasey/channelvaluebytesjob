##Docker configuration

Build the docker image:

$ mvn clean compile package docker:build

View the built images:

$ docker images

Start the container in the background. The environment variables need to be set (see the required environment variables below)

Syntax:

$ docker run -d -e FLEET=SNG -e DB_URL=<DB_URL> -e DB_INSTANCE=<DB_INSTANCE> -e DB_NAME_SLT=<DB_NAME_SLT> -e DB_NAME_VIRM=<DB_NAME_VIRM> -e DB_NAME_SNG=<DB_NAME_SNG> -e DB_USER=<DB_USER> -e DB_PWD=<DB_PWD> <IMAGE_ID>          

Example:
$ docker run -d -e FLEET=SNG -e DB_URL=192.168.99.100 -e DB_INSTANCE=sql2012 -e DB_NAME_SLT=NedTrain_Spectrum_SLT -e DB_NAME_VIRM=NedTrain_Spectrum_VIRM -e DB_NAME_SNG=NedTrain_Spectrum_SNG -e DB_USER=masteruser -e DB_PWD=mypassword <IMAGE_ID>          

View the running containers

$ docker ps

Access the docker container while it is running:

$ docker exec -i -t CONTAINER_ID /bin/bash

Copy the logs from the container:

$ docker cp CONTAINER_ID:/var/log/channelvaluebytesjob.log ./

after container has finished view the logs:

$ docker logs <CONTAINER_ID>

###Docker Environment Variables

FLEET: 			Fleet for which we are updating the channel values (currently only SNG)

DB_URL:			The r2m database host

DB_INSTANCE:	The name of the r2m database instance if any. Omit if not required.

DB_USER:		The r2m database user.

DB_PWD: 		The r2m database password.

DB_NAME_SLT: 	The r2m db name for the SLT fleet.

DB_NAME_VIRM: 	The r2m db name for the VIRM fleet.

DB_NAME_SNG: 	The r2m db name for the SNG fleet.

## For developers on local windows env 
build the app from regular cmd window:

->mvn clean & mvn compile & mvn install -Dmaven.test.skip=true

set the following environment variables:

DB_URL			=  10.27.81.107,

DB_NAME_SLT		=  NS_Dev_Spectrum,

DB_NAME_VIRM	=  NS_Dev_Spectrum_VIRM,

DB_NAME_SNG		=  NS_Dev_Spectrum_SNG,

DB_INSTANCE		= {not required for QA env},

DB_USER			= spectrumAppUserDev,

DB_PWD			= N1e2x345$


run the channelvaluebytesjob using the bat file

it accepts the following parameter:

FLEET 			= SNG/SLT/VIRM (only used for SNG currently)

Example:

->run.bat SNG

###Windows Environment Variables

DB_URL:			The r2m database host

DB_INSTANCE:	The name of the r2m database instance if any. Omit if not required.

DB_USER:		The r2m database user.

DB_PWD: 		The r2m database password.

DB_NAME_SLT: 	The r2m db name for the SLT fleet.

DB_NAME_VIRM: 	The r2m db name for the VIRM fleet.

DB_NAME_SNG: 	The r2m db name for the SNG fleet.
